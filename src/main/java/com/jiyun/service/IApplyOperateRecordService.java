package com.jiyun.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.ApplyOperateRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.vo.ApplyOperateRecordVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
public interface IApplyOperateRecordService extends IService<ApplyOperateRecord> {

    Page<ApplyOperateRecordVo> findPage(Integer page, Integer pageSize, ApplyOperateRecordVo applyOperateRecordVo);

    List<ApplyOperateRecordVo> findDetailList(Integer aid);
}
