package com.jiyun.service.impl;

import com.jiyun.entity.Type;
import com.jiyun.mapper.TypeMapper;
import com.jiyun.service.ITypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements ITypeService {

}
