package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.ApplyOperateRecord;
import com.jiyun.mapper.ApplyOperateRecordMapper;
import com.jiyun.service.IApplyOperateRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.vo.ApplyOperateRecordVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
@Service
public class ApplyOperateRecordServiceImpl extends ServiceImpl<ApplyOperateRecordMapper, ApplyOperateRecord> implements IApplyOperateRecordService {

    @Autowired
    ApplyOperateRecordMapper applyOperateRecordMapper;
    @Override
    public Page<ApplyOperateRecordVo> findPage(Integer page, Integer pageSize, ApplyOperateRecordVo applyOperateRecordVo) {
        Page<ApplyOperateRecordVo> applyOperateRecordVoPage = new Page<>(page, pageSize);
        applyOperateRecordMapper.findPage(applyOperateRecordVoPage,applyOperateRecordVo);

        return applyOperateRecordVoPage;
    }

    @Override
    public List<ApplyOperateRecordVo> findDetailList(Integer aid) {
        return applyOperateRecordMapper.findDetailList(aid);
    }
}
