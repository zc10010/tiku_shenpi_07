package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Apply;
import com.jiyun.mapper.ApplyMapper;
import com.jiyun.service.IApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.vo.ApplyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
@Service
public class ApplyServiceImpl extends ServiceImpl<ApplyMapper, Apply> implements IApplyService {
    @Autowired
    ApplyMapper applyMapper;


    @Override
    public Page<ApplyVo> findPage(Integer page, Integer pageSize, Apply apply) {

        Page<ApplyVo> applyVoPage = new Page<>(page, pageSize);
        applyMapper.findPage(applyVoPage,apply);
        return applyVoPage;
    }

    @Override
    public Page<ApplyVo> findPage4Copy(Integer page, Integer pageSize, ApplyVo applyVo) {

        Page<ApplyVo> applyVoPage = new Page<>(page, pageSize);
        applyMapper.findPage4Copy(applyVoPage,applyVo);
        return applyVoPage;
    }
}
