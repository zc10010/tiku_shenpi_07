package com.jiyun.service;

import com.jiyun.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
public interface IUserService extends IService<User> {

}
