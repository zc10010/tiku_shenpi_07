package com.jiyun.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Apply;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.vo.ApplyVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
public interface IApplyService extends IService<Apply> {

    Page<ApplyVo> findPage(Integer page, Integer pageSize, Apply apply);

    Page<ApplyVo> findPage4Copy(Integer page, Integer pageSize, ApplyVo applyVo);
}
