package com.jiyun.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Apply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiyun.vo.ApplyVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
public interface ApplyMapper extends BaseMapper<Apply> {

    Page<ApplyVo> findPage(Page<ApplyVo> applyVoPage, @Param("a") Apply apply);

    Page<ApplyVo> findPage4Copy(Page<ApplyVo> applyVoPage,@Param("a") ApplyVo applyVo);
}
