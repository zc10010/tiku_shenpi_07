package com.jiyun.mapper;

import com.jiyun.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
public interface UserMapper extends BaseMapper<User> {

}
