package com.jiyun.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.ApplyOperateRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiyun.vo.ApplyOperateRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
public interface ApplyOperateRecordMapper extends BaseMapper<ApplyOperateRecord> {

    Page<ApplyOperateRecordVo> findPage(Page<ApplyOperateRecordVo> applyOperateRecordVoPage,@Param("a") ApplyOperateRecordVo applyOperateRecordVo);

    List<ApplyOperateRecordVo> findDetailList(@Param("aid") Integer aid);
}
