package com.jiyun.vo;

import com.jiyun.entity.Apply;
import lombok.Data;

import java.util.Date;

@Data
public class ApplyVo extends Apply {


    private String uname;
    private String adminName;
    private String copyName;
    private String tname;

    private Date startTime;
    private Date endTime;




}
