package com.jiyun.vo;

import com.jiyun.entity.ApplyOperateRecord;
import lombok.Data;

import java.util.Date;

@Data
public class ApplyOperateRecordVo extends ApplyOperateRecord {


    //查询条件扩展
    private String uname;
    private Date startTime;
    private Date endTime;
    private Integer tid;

    //响应参数
    private String intro;
    private String tname;
    private String adminName;
    private String nextAdminName;



}
