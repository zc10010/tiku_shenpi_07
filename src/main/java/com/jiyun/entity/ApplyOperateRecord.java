package com.jiyun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApplyOperateRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer aid;

    private Integer adminId;

    private Date createTime;

    private Date operateTime;

    private String remark;

    private String state;

    private Integer nextAdminId;

    @TableField(exist = false)
    private String astate;


}
