package com.jiyun.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.jiyun.entity.Type;
import com.jiyun.service.ITypeService;
import java.util.List;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
@RestController
@RequestMapping("/type")
public class TypeController {

    @Autowired
    ITypeService typeService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addType(@RequestBody Type type){
        typeService.save(type);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateType(@RequestBody Type type){
        typeService.updateById(type);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        typeService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        typeService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Type findById(@RequestParam Integer id){
        return typeService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Type> findAll(){
        return typeService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Type> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<Type> typePage = new Page<>(page, pageSize);
        return typeService.page(typePage);
    }

}
