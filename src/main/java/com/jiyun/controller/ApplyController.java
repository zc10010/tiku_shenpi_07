package com.jiyun.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Apply;
import com.jiyun.entity.ApplyOperateRecord;
import com.jiyun.service.IApplyOperateRecordService;
import com.jiyun.service.IApplyService;
import com.jiyun.vo.ApplyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
@RestController
@RequestMapping("/apply")
public class ApplyController {

    @Autowired
    IApplyService applyService;

    @Autowired
    IApplyOperateRecordService applyOperateRecordService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addApply(@RequestBody Apply apply){
        apply.setCreateTime(new Date());
        apply.setState("待审批");
        applyService.save(apply);

        ApplyOperateRecord applyOperateRecord = new ApplyOperateRecord();
        applyOperateRecord.setAid(apply.getId());
        applyOperateRecord.setAdminId(apply.getAdminUid());
        applyOperateRecord.setCreateTime(new Date());
        applyOperateRecord.setState("审核中");
        applyOperateRecordService.save(applyOperateRecord);

    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateApply(@RequestBody Apply apply){
        applyService.updateById(apply);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        applyService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        applyService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Apply findById(@RequestParam Integer id){
        return applyService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Apply> findAll(){
        return applyService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<ApplyVo> findPage(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestBody Apply apply){

        return applyService.findPage(page,pageSize,apply);
    }
    @RequestMapping("/findPage4Copy")
    public Page<ApplyVo> findPage4Copy(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestBody ApplyVo applyVo){

        return applyService.findPage4Copy(page,pageSize,applyVo);
    }

}
