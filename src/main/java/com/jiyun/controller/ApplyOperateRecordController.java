package com.jiyun.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Apply;
import com.jiyun.entity.ApplyOperateRecord;
import com.jiyun.service.IApplyOperateRecordService;
import com.jiyun.service.IApplyService;
import com.jiyun.vo.ApplyOperateRecordVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-05-23
 */
@RestController
@RequestMapping("/apply-operate-record")
public class ApplyOperateRecordController {

    @Autowired
    IApplyOperateRecordService applyOperateRecordService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addApplyOperateRecord(@RequestBody ApplyOperateRecord applyOperateRecord){
        applyOperateRecordService.save(applyOperateRecord);
    }

    @Autowired
    IApplyService applyService;

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateApplyOperateRecord(@RequestBody ApplyOperateRecord applyOperateRecord){

        ApplyOperateRecord byId = applyOperateRecordService.getById(applyOperateRecord.getId());

        applyOperateRecord.setOperateTime(new Date());
        applyOperateRecordService.updateById(applyOperateRecord);

        if(applyOperateRecord.getState().equals("继续审批")){
            ApplyOperateRecord next = new ApplyOperateRecord();
            next.setState("审核中");
            next.setAid(byId.getAid());
            next.setAdminId(applyOperateRecord.getNextAdminId());
            next.setCreateTime(new Date());

            applyOperateRecordService.save(next);

        }else if(applyOperateRecord.getState().equals("审核结束")){
            Apply apply = applyService.getById(byId.getAid());
            apply.setState(applyOperateRecord.getAstate());
            applyService.updateById(apply);
        }




    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        applyOperateRecordService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        applyOperateRecordService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public ApplyOperateRecord findById(@RequestParam Integer id){
        return applyOperateRecordService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<ApplyOperateRecord> findAll(){
        return applyOperateRecordService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<ApplyOperateRecordVo> findPage(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestBody ApplyOperateRecordVo applyOperateRecordVo){

        return applyOperateRecordService.findPage(page,pageSize,applyOperateRecordVo);
    }

    @RequestMapping("findDetailList")
    public List<ApplyOperateRecordVo> findDetailList(Integer aid){
        return applyOperateRecordService.findDetailList(aid);
    }

}
